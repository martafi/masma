﻿using System;
using System.Linq;

namespace PredatorPrey
{
    public class Utils
    {
        public static int GridSize = 16;
        public static int NoTurns = 1000;
        public static int NoSheep = 32;
        public static int NoWolves = 16;
        public static bool Verbose = false;
        public static Random Rand = new Random();
        public static int DelayBetweenTurns = 100;
        public static int NoTurnsUntilSheepBreeds = 5;
        public static int NoTurnsUntilWolfBreeds = 20;
        public static int NoTurnsUntilWolfStarves = 20;

        public static int[] RandomPermutation(int n)
        {
            int[] numbers = new int[n];
            for (int i = 0; i < n; i++)
                numbers[i] = i;
            int[] randPerm = numbers.OrderBy(x => Rand.Next()).ToArray();
            return randPerm;
        }
    }
}