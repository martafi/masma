﻿namespace PredatorPrey
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var worldEnv = new World(); // derived from ActressMas.Environment();

            int noAgents = Utils.GridSize * Utils.GridSize;

            worldEnv.InitWorldMap();

            int[] randVect = Utils.RandomPermutation(noAgents);

            for (int i = 0; i < Utils.NoWolves; i++)
            {
                var a = new WolfAgent();
                worldEnv.Add(a, worldEnv.CreateName(a)); // unique name
                worldEnv.AddAgentToMap(a, randVect[i]);
                a.Start();
            }

            for (int i = Utils.NoWolves; i < Utils.NoWolves + Utils.NoSheep; i++)
            {
                var a = new SheepAgent();
                worldEnv.Add(a, worldEnv.CreateName(a));
                worldEnv.AddAgentToMap(a, randVect[i]);
                a.Start();
            }

            
            
            var s = new SchedulerAgent();
            worldEnv.Add(s, "scheduler");
            s.Start();

            worldEnv.WaitAll();
        }
    }
}