﻿using System.Collections.Generic;
using System.Threading;

namespace ContractNet
{
    class Program
    {
        static void Main(string[] args)
        {
            var env = new ActressMas.Environment();
            var initiators = new List<Initiator>();
            for (int i = 0; i < Utils.NoInitiators; ++i)
            {
                var initiatorAgent = new Initiator(); env.Add(initiatorAgent, "initiator" + i);
                initiators.Add(initiatorAgent);
            }
            var participantAgent = new Participant(); env.Add(participantAgent, "participant");

            participantAgent.Start();
            Thread.Sleep(100);
            foreach (var agent in initiators)
            {
                agent.Start();
            }

            env.WaitAll();
        }
    }
}
