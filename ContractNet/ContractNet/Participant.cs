﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Timers;

namespace ContractNet
{
    class Product
    {
        private Random random = new Random();
        private string name;
        public int quantity { get; set; }
        private Timer timer { get; }
        public double price { get; set; }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            price *= 0.095;
        }

        public Product(string _name, int _quantity, double _price)
        {
            name = _name;
            quantity = _quantity;
            price = _price;
            timer = new System.Timers.Timer();
            timer.Interval = 700 + random.Next(3) * 100;
            timer.Enabled = true;
            timer.AutoReset = true;
            timer.Elapsed += OnTimedEvent;
        }
    }

    public class Participant : Agent
    {
        float probabilityToSendProposal = 0.8F;
        float probabilityToSolveProposal = 0.5F;
        private Dictionary<string, Product> _products = new Dictionary<string, Product>();

        public override void Setup()
        {
            _products.Add("apple", new Product("apple", 20, 2.5) );
            _products.Add("water", new Product("water", 50, 1));
            _products.Add("potatoes", new Product("potatoes", 6, 1.2));
            _products.Add("shoes", new Product("shoes", 20, 70));
            _products.Add("biscuites", new Product("biscuites", 40, 10));
            _products.Add("pasta", new Product("pasta", 18, 20));
        }

        public override void BeforeStop()
        {
            Send("initiator", "[Stop]");
        }

        public override void Act(Message message)
        {
            string action;
            List<string> parameters;
            Utils.ParseMessage(message.Content, out action, out parameters);
            Console.WriteLine("\t[{0}]: received {1} from [{2}] ", this.Name, action, message.Sender);

            switch (action)
            {
                case "[Call-for-proposals]":
                    string product = parameters[0];
                    int quantity = Convert.ToInt32(parameters[1]) + 1;
                    Console.WriteLine("{0} {1}", product, quantity);
                    if (_products[product].quantity >= quantity)
                    {
                        Send(message.Sender, Utils.Str("[Propose]", product, quantity, Convert.ToString(_products[product].price)));
                    }
                    else
                    {
                        Send(message.Sender, "[Refuse]");
                    }
                    break;

                case "[Accept-proposal]":
                    product = parameters[0];
                    quantity = Convert.ToInt32(parameters[1]) + 1;
                    if (_products[product].quantity >= quantity)
                    {
                        _products[product].quantity -= quantity;
                        Send(message.Sender, Utils.Str("[Inform-done]", parameters[0]) );
                    }
                    else
                    {
                        Send(message.Sender, "[Failure]");
                    }
                    break;

                case "[Reject-proposal]":
                    Console.WriteLine("\t[{0}]: [{1}] rejected my proposal. Communication ended.",
                        this.Name, message.Sender);
                    break;
            }

        }
    }
}
