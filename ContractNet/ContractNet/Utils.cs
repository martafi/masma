﻿using System;
using System.Collections.Generic;

namespace ContractNet
{
    public class Utils
    {
        public static int Delay = 100;
        public static Random RandNoGen = new Random();

        public static int NoInitiators = 3;

        public static bool EventOccurs(float probability)
        {
            return RandNoGen.NextDouble() <= probability;
        }

        public static void ParseMessage(string content, out string action, out List<string> parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = new List<string>();
            for (int i = 1; i < t.Length; i++)
                parameters.Add(t[i]);
        }

        public static void ParseMessage(string content, out string action, out string parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = "";

            if (t.Length > 1)
            {
                for (int i = 1; i < t.Length - 1; i++)
                    parameters += t[i] + " ";
                parameters += t[t.Length - 1];
            }
        }

        public static string Str(string p1, double p2)
        {
            return string.Format("{0} {1:F1}", p1, p2);
        }

        public static string Str(string p1, string p2, int p3)
        {
            return string.Format("{0} {1} {2}", p1, p2, p3);
        }

        public static string Str(string p1, string p2)
        {
            return string.Format("{0} {1}", p1, p2);
        }

        public static string Str(string p1, string p2, int p3, string p4)
        {
            return string.Format("{0} {1} {2} {3}", p1, p2, p3, p4);
        }

    }
}
