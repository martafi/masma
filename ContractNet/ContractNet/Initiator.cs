﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
using System.Threading;

namespace ContractNet
{
    public class InitiatorProduct
    {
        private System.Timers.Timer timer;
        private Initiator initiator;
        private string name;
        public bool boughtFromLastTime { get; set; }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (!boughtFromLastTime)
            {
                if (initiator.products.ContainsKey(name)) {
                    initiator.products.Remove(name);
                } 
                if(initiator.products.Count == 0)
                {
                    initiator.Stop();
                }
            }
            boughtFromLastTime = false;
        }

        public InitiatorProduct(string _name, Initiator _initiator)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 2000;
            name = _name;
            timer.Elapsed += OnTimedEvent;
            boughtFromLastTime = false;
            initiator = _initiator;
            timer.Enabled = true;
            timer.AutoReset = true;
        }
    }

    public class Initiator : Agent
    {
        float probabilityToAcceptProposal = 0.7F;
        public Dictionary<string, InitiatorProduct> products { get; }
        Random _random = new Random();

        public Initiator()
        {
            products = new Dictionary<string, InitiatorProduct>();
            products.Add("apple", new InitiatorProduct("apple", this));
            products.Add("water", new InitiatorProduct("water", this));
            products.Add("potatoes", new InitiatorProduct("potatoes", this));
            products.Add("shoes", new InitiatorProduct("shoes", this));
            products.Add("biscuites", new InitiatorProduct("biscuites", this));
            products.Add("pasta", new InitiatorProduct("pasta", this));
        }

        public override void Setup()
        {
            Console.WriteLine("[{0}]: Sending proposal to participant.", this.Name);
            int random_item = _random.Next(products.Count);
            //Console.WriteLine("**********************   {0} {1}", products.Count(), random_item);
            int random_quantity = _random.Next(20);
            Send("participant", Utils.Str("[Call-for-proposals]", products.Keys.ToList()[random_item], random_quantity));
        }

        public override void Act(Message message)
        {
            string action;
            List<string> parameters;
            Utils.ParseMessage(message.Content, out action, out parameters);
            
            Console.WriteLine("[{0}]: received {1} from [{2}]", this.Name, action, message.Sender);

            switch (action)
            {
                case "[Propose]":
                    string product = parameters[0];
                    int quantity = Convert.ToInt32(parameters[1]);
                    Console.WriteLine("{0} {1}", product, quantity);
                    Send(message.Sender, Utils.Str("[Accept-proposal]", product, quantity));
                    break;

                case "[Refuse]":
                    Console.WriteLine("\t[{0}]: [{1}] refused to make a proposal. Communication ended.",
                        this.Name, message.Sender);
                    Setup();
                    break;

                case "[Inform-done]":
                    Console.WriteLine("\t[{0}]: [{1}] solved the problem. Communication ended.",
                        this.Name, message.Sender);
                    products[parameters[0]].boughtFromLastTime = true;
                    Setup();
                    break;

                case "[Failure]":
                    Console.WriteLine("\t[{0}]: [{1}] failed to solve the problem. Communication ended.",
                        this.Name, message.Sender);
                    Setup();
                    break;
                case "[Stop]":
                    Stop();
                break;
            }
        }
    }
}




