﻿using System.Collections.Generic;
using System.Threading;

namespace IteratedPrisonersDilemma
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var env = new ActressMas.Environment();
            for (int i = 1; i <= Utils.NoPlayers; ++i)
            {
                var prisonerAgent = new PrisonerAgent();
                env.Add(prisonerAgent, "prisoner" + i);
                prisonerAgent.Start();
            }

            Thread.Sleep(100);

            var policeAgent = new PoliceAgent();
            env.Add(policeAgent, "police");
            policeAgent.Start();

            env.WaitAll();
        }
    }
}