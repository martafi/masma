﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Timers;

namespace IteratedPrisonersDilemma
{
    public class PoliceAgent : Agent
    {
        private Dictionary<string, int> turns = new Dictionary<string, int>();
        private int roomNumber = 0;
        private Dictionary<string, Dictionary<string, string> > responses;
        private Dictionary<string, string> playersRoom;
        private System.Timers.Timer timer;

        public PoliceAgent()
        {
            responses = new Dictionary<string, Dictionary<string, string>>();
            playersRoom = new Dictionary<string, string>();

            for (int i = 1; i <= Utils.NoPlayers; ++i)
            {
                playersRoom.Add("prisoner" + i, string.Empty);
            }

            timer = new System.Timers.Timer();
            timer.Interval = 2000;
            timer.Enabled = true;
            timer.AutoReset = true;
            timer.Elapsed += OnTimedEvent;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            CreateRooms();
            
        }

        void CreateRoom(string player1, string player2)
        {
            roomNumber++;
            if (roomNumber <= Utils.NoRooms)
            {
                string room = "room" + roomNumber;

                turns.Add(room, 1);
                responses[room] = new Dictionary<string, string>();

                Send(player1, "new");
                Send(player2, "new");

                Send(player1, Utils.Str("turn", room, string.Empty));
                Send(player2, Utils.Str("turn", room, string.Empty));

                Console.WriteLine("[{0}] Turn {1} for room {2} ", this.Name, turns[room], roomNumber);
            } else
            {
                int free_players = playersRoom.Where(pair => pair.Value == string.Empty).ToList().Count();
                if (free_players == Utils.NoPlayers)
                {
                    Thread.Sleep(2000);
                    BroadcastAll("stop");
                    Stop();
                }
                
            }
        }

        public void CreateRooms()
        {
            List<string> free_players = new List<string>();
            foreach (var pair in playersRoom)
                if (pair.Value == string.Empty)
                {
                    free_players.Add(pair.Key);               
                }
            Random random = new Random();
            free_players = free_players.OrderBy(a => random.Next(100000)).ToList();
            for (int i = 0; i < free_players.Count; i += 2)
            {
                CreateRoom(free_players[i], free_players[i + 1]);
            }
            
        }

        public override void Setup()
        {
            CreateRooms();
        }

        public override void Act(Message message)
        {
            try
            {
                //Console.WriteLine("\t[{1} -> {0}]: {2}", this.Name, message.Sender, message.Content);

                string action;
                List<string> parameters;
                Utils.ParseMessage(message.Content, out action, out parameters);

                switch (action)
                {
                    case "play":
                        HandlePlay(message.Sender, parameters[0], parameters[1].ToLower());
                        break;

                    case "stop":
                        Stop();
                        break;


                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void HandlePlay(string sender, string room, string action)
        {
            responses[room].Add(sender, action);

            if (responses[room].Count == 2)
            {
                var e = responses[room].GetEnumerator();
                e.MoveNext(); var response1 = e.Current;
                e.MoveNext(); var response2 = e.Current;

                int outcome1, outcome2;
                ComputeOutcome(response1.Value, response2.Value, out outcome1, out outcome2);

                Send(response1.Key, Utils.Str("outcome", outcome1, response2.Value));
                Send(response2.Key, Utils.Str("outcome", outcome2, response1.Value));

                responses[room].Clear();

                turns[room]++;
                if (turns[room] == Utils.NoTurns)
                {
                    Thread.Sleep(Utils.Delay);

                    Console.WriteLine("[{0}] Room {1} finished", this.Name, room);

                    Send(response1.Key, Utils.Str("end", room));
                    Send(response2.Key, Utils.Str("end", room));

                    responses.Remove(room);
                    playersRoom[response1.Key] = string.Empty;
                    playersRoom[response2.Key] = string.Empty;
                    turns.Remove(room);
                }
                else
                {
                    Send(response1.Key, Utils.Str("turn", room, response2.Value));
                    Send(response2.Key, Utils.Str("turn", room, response1.Value));
                }
            }
        }

        private void ComputeOutcome(string action1, string action2, out int outcome1, out int outcome2)
        {
            outcome1 = outcome2 = 0;

            if (action1 == "confess" && action2 == "confess")
            {
                outcome1 = outcome2 = -3;
            }
            else if (action1 == "deny" && action2 == "deny")
            {
                outcome1 = outcome2 = -1;
            }
            else if (action1 == "confess" && action2 == "deny")
            {
                outcome1 = 0;
                outcome2 = -5;
            }
            else if (action1 == "deny" && action2 == "confess")
            {
                outcome1 = -5;
                outcome2 = 0;
            }
        }
    }
}