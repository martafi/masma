﻿using ActressMas;
using System;
using System.Collections.Generic;

namespace IteratedPrisonersDilemma
{
    public class PrisonerAgent : Agent
    {
        protected int points = 0;
        protected int lastOutcome = 0;
        public List<Utils.ChooseAction> actions = new List<Utils.ChooseAction>();
        
        public Utils.ChooseAction Strategy = new Utils.ChooseAction(Utils.TitTatAction);
        private Random random;

        public PrisonerAgent()
        {
            actions.Add(new Utils.ChooseAction(Utils.RandomAction));
            actions.Add(new Utils.ChooseAction(Utils.ConfessAction));
            actions.Add(new Utils.ChooseAction(Utils.DenyAction));
            actions.Add(new Utils.ChooseAction(Utils.TitTatAction));
            random = new Random();
        }

        public override void Act(Message message)
        {
            try
            {
                //Console.WriteLine("\t[{1} -> {0}]: {2}", this.Name, message.Sender, message.Content);

                string action;
                List<string> parameters;
                Utils.ParseMessage(message.Content, out action, out parameters);

                switch (action)
                {
                    case "turn":
                        HandleTurn(parameters[0], parameters[1]);
                        break;

                    case "outcome":
                        HandleOutcome(Convert.ToInt32(parameters[0]));
                        break;

                    case "end":
                        HandleEnd();
                        break;

                    case "stop":
                        Stop();
                        break;

                    case "new":
                        Strategy = actions[random.Next(action.Length)];
                        points = 0;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void HandleTurn(string room, string lastOponentsMove)
        {
            Action action = Strategy(lastOutcome, lastOponentsMove);
            Send("police", Utils.Str("play", room, Enum.GetName(typeof(Action), action)));
        }

        private void HandleOutcome(int outcome)
        {
            lastOutcome = outcome;
            points += lastOutcome;
        }

        private void HandleEnd()
        {
            Console.WriteLine("[{0}]: I have {1} points and I used {2} strategy", this.Name, points, Strategy.Method.Name);
        }
    }
}