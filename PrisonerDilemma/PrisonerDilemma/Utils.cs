﻿using System;
using System.Collections.Generic;

namespace IteratedPrisonersDilemma
{
    public enum Action
    {
        Confess = 0,
        Deny = 1
    }

    public class Utils
    {
        public static int NoTurns = 10;
        public static int NoPlayers = 4;

        public static int Delay = 1000;
        public static Random RandNoGen = new Random();
        internal static int NoRooms = 10;

        public delegate Action ChooseAction(int outcome, string opponentsMove);

        public static Action RandomAction(int outcome, string opponentsMove)
        {
            if (Utils.RandNoGen.NextDouble() < 0.5)
                return Action.Confess;
            else
                return Action.Deny;
        }

        public static Action ConfessAction(int outcome, string opponentsMove)
        {
            return Action.Confess;
        }

        public static Action DenyAction(int outcome, string opponentsMove)
        {
            return Action.Deny;
        }

        public static Action TitTatAction(int outcome, string oppponentsMove)
        {
            return oppponentsMove == Action.Deny.ToString().ToLower() ? Action.Deny : Action.Confess;
        }

        public static void ParseMessage(string content, out string action, out string parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = "";

            if (t.Length > 1)
            {
                for (int i = 1; i < t.Length - 1; i++)
                    parameters += t[i] + " ";
                parameters += t[t.Length - 1];
            }
        }

        public static void ParseMessage(string content, out string action, out List<string> parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = new List<string>();
            for (int i = 1; i < t.Length; i++)
                parameters.Add(t[i]);
        }

        public static string Str(object p1, object p2)
        {
            return string.Format("{0} {1}", p1, p2);
        }

        public static string Str(object p1, object p2, object p3)
        {
            return string.Format("{0} {1} {2}", p1, p2, p3);
        }

        public static string Str(object p1, object p2, object p3, object p4)
        {
            return string.Format("{0} {1} {2} {3}", p1, p2, p3, p4);
        }
    }
}