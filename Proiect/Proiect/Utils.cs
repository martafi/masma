﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    /*
     * This class is an util that helps to get the values sorted even if they are equal when using a sorted set.   
     */
    class MyUniqueValue : IComparable<MyUniqueValue>
    {
        public int value { get; }
        public int chunk { get; }
        public MyUniqueValue(int value, int bucket)
        {
            this.value = value;
            this.chunk = bucket;
        }

        public int CompareTo(MyUniqueValue other)
        {
            if (value < other.value || value == other.value && chunk < other.chunk) return -1;
            if (value == other.value && chunk == other.chunk) return 0;
            return 1;
        }
    }

    class Utils
    {
        public const string POSITION = "POSITION";
        public const string ORDER = "ORDER";
        public const string DONE = "DONE";
        public const string COUNTING_AGENT_PREFIX = "Simple";

        public static int LOG_LEVEL = 1;
        public static int DESIRED_NUMBER_OF_AGENTS = 10;
        public static int NO_NUMBERS = 10000;
        public static int NUMBER_OF_ACTIVE_AGENTS = 3;

        public static void ParseMessage(string content, out string action, out List<string> parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = new List<string>();
            for (int i = 1; i < t.Length; i++)
                parameters.Add(t[i]);
        }

        public static string GetString(string prefix, int number)
        {
            return string.Format("{0}{1}", prefix, number);
        }

        public static string GetString(string command, int start, int end)
        {
            return string.Format("{0} {1} {2}", command, start, end);
        }

        public static void Log(string a, string b, string c)
        {
            if (Utils.LOG_LEVEL == 2)
            {
                Console.WriteLine(a, b, c);
            }
        }

        /* @Returns a list that contains size element with value 0.                                     
         */
        public static List<int> CreateFixedSizeList(int size)
        {
            List<int> myList = new List<int>(size);
            myList.AddRange(Enumerable.Repeat(0, size));
            return myList;
        }

        public static void Display(List<int> list)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                Console.Write("{0} ", list[i]);
            }
            Console.WriteLine();
        }
    }
}
