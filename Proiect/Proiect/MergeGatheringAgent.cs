﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    /* This type of gathering agent combines enumeration sort with the merging of
     * multiple lists for improving enumertation sort algorithm performance
     */   
    class MergeGatheringAgent : GatheringAgent
    {
        public MergeGatheringAgent(List<int> numbers, int desiredNumberOfAgents, int numberOfActiveAgents) : 
            base(numbers, desiredNumberOfAgents, numberOfActiveAgents) {}

        public override void HandleDone()
        {
            Broadcast(Utils.DONE);
            SortedSet< MyUniqueValue > ordered = new SortedSet<MyUniqueValue >();
            List<int> indexes = Utils.CreateFixedSizeList(startChunk.Count);
            List<int> merged_lists = new List<int>();

            // get the lowest values in each chunk
            for (int i = 0; i < startChunk.Count; ++i)
            {
                ordered.Add(new MyUniqueValue(sortedList[startChunk[i]], i));
                indexes[i] = startChunk[i];
            }

            // merge the values found in the numbers.Count / desiredNumberOfAgents sorted chunks
            while (merged_lists.Count < sortedList.Count)
            {
                var min = ordered.First();
                merged_lists.Add(min.value);

                var x = ordered.Remove(min);
                int chunk = min.chunk;

                indexes[chunk]++;
                if (indexes[chunk] < endChunk[chunk])
                {
                    ordered.Add(new MyUniqueValue(sortedList[ indexes[chunk] ], chunk));
                }
            }
            this.sortedList = merged_lists;
            Stop();
        }
    }
}
