﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    /*
     * This type of agent handles the following types of messages:
     * ORDER left_position right_position: when such a message will be received
     *                                     the position in the sorted sublist found 
     *                                     between position [left_position, right_position)
     *                                     will be comunicated to the gathering agent.    
     * DONE                              : will terminated the agent    
     */
    class BoxedCountingAgent : SimpleCountingAgent
    {
        private readonly List<int> numbers;

        public BoxedCountingAgent(List<int> numbers) : base(numbers)
        {
        }

        public override void Act(Message message)
        {
            string operation;
            List<string> operation_params = new List<string>();

            Utils.ParseMessage(message.Content, out operation, out operation_params);

            Utils.Log("From {0} --> message {1}", message.Sender, message.Content);

            switch (operation)
            {
                case Utils.ORDER:
                    int start = Convert.ToInt32(operation_params[0]);
                    int end = Convert.ToInt32(operation_params[1]);
                    for (int i = start; i < end; ++i)
                    {
                        int position = start + findOrder(i, start, end);
                        Send("gathering", Utils.POSITION + " " + i + " " + position);
                    }
                    Send("gathering", Utils.DONE);
                    break;
                case Utils.DONE:
                    Stop();
                    break;
            }
        }
    }
}
