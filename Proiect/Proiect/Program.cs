﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Proiect
{
    class Program
    {
        static void Main(string[] args)
        {
            Utils.NO_NUMBERS = 100000;//Convert.ToInt32(args[0]);
            Utils.NUMBER_OF_CHUNCKS = 50;//Convert.ToInt32(args[1]);
            Utils.NUMBER_OF_ACTIVE_AGENTS = 10;//Convert.ToInt32(args[2]);
            Console.WriteLine("The parameters are {0} numbers and {1} agents and {2} of them are active", 
                              Utils.NO_NUMBERS, 
                              Utils.NUMBER_OF_CHUNCKS,
                              Utils.NUMBER_OF_ACTIVE_AGENTS);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            List<Agent> agents = new List<Agent>();

            var env = new ActressMas.Environment();
            var numbers = new List<int>(Utils.NO_NUMBERS);

            generateNumbers(numbers);

            GatheringAgent gatheringAgent = new MergeGatheringAgent(numbers, Utils.NUMBER_OF_CHUNCKS, Utils.NUMBER_OF_ACTIVE_AGENTS);
            agents.Add(gatheringAgent);
            env.Add(gatheringAgent, "gathering");


            agents.InsertRange(0, CreateBoxedAgents(env, numbers)); 


            foreach (var agent in agents)
            {
                agent.Start();
            }

            env.WaitAll();

            watch.Stop();

            long time = watch.ElapsedMilliseconds;

            if ((gatheringAgent != null) && (Utils.LOG_LEVEL > 0))
            {
                Console.Write("Before Sorting: ");
                Utils.Display(numbers);

                Console.Write("After Sorting: ");
                Utils.Display(gatheringAgent.sortedList);

                Console.WriteLine("Sorting time {0}", time);
            }
            StreamWriter outputFile = new StreamWriter("/Users/marta/Desktop/MSMA/masma/Proiect/Proiect/results.txt", true);
            outputFile.WriteLine("{0},{1},{2}", Utils.NO_NUMBERS, Utils.NUMBER_OF_CHUNCKS, time);
            outputFile.Close();
        }

        private static List<Agent> CreateBoxedAgents(ActressMas.Environment env, List<int> numbers)
        {
            List<Agent> agents = new List<Agent>();
            for (int i = 0; i < Utils.NUMBER_OF_CHUNCKS; ++i)
            {
                Agent agent = new BoxedCountingAgent(numbers);
                env.Add(agent, Utils.GetString(Utils.COUNTING_AGENT_PREFIX, i));
                agents.Add(agent);
            }
            return agents;
        }

        private static List<Agent> CreateSimpleAgents(ActressMas.Environment env, List<int> numbers)
        {
            List<Agent> agents = new List<Agent>();
            for (int i = 0; i < Utils.NUMBER_OF_CHUNCKS; ++i)
            {
                Agent agent = new SimpleCountingAgent(numbers);
                env.Add(agent, Utils.GetString(Utils.COUNTING_AGENT_PREFIX, i));
                agents.Add(agent);
            }
            return agents;
        }

        private static void generateNumbers(List<int> numbers)
        {
            Random random = new Random();
            for (int i = 0; i < Utils.NO_NUMBERS; ++i)
            {
                numbers.Add(random.Next(20));
            }
        }
    }
}
