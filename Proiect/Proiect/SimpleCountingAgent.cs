﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    /*
     * This agent main aim is to count how many values are lower than a given 
     * value. It handles the following types of messages:
     * ORDER left_position right_position: when such a message will be received
     *                                     ,from the gathering agent, he position in 
     *                                     the sorted list for all the elements found 
     *                                     between [left_position, right_position)
     *                                     will be comunicated to the gathering agent.    
     * DONE                              : will stop the agent    
     */
    class SimpleCountingAgent : Agent
    {
        protected readonly List<int> numbers;

        public SimpleCountingAgent(List<int> numbers)
        {
            this.numbers = numbers;
        }

        public override void Act(Message message)
        {
            string operation;
            List<string> operation_params = new List<string>();

            Utils.ParseMessage(message.Content, out operation, out operation_params);

            switch (operation)
            {
                case Utils.ORDER:
                    Utils.Log("From {0} --> message: {1}", message.Sender, message.Content);
                    int start = Convert.ToInt32(operation_params[0]);
                    int end = Convert.ToInt32(operation_params[1]);
                    for  (int i = start; i < end; ++i)
                    {
                        int position = findOrder(i, 0, numbers.Count);
                        Send("gathering", Utils.POSITION + " " + i + " " + position);
                    }
                    Send("gathering", Utils.DONE);
                    break;
                case Utils.DONE:
                    Stop();
                    break;
            }
        }

        /*
         * The function computes how many elements with their positions between 
         * start and end are lower than or equal to the element found at position 
         * elem_position, or are equal to it but have their position lower 
         * than elem_position
         */
        protected int findOrder(int elem_position, int start, int end)
        {
            int position = 0;
            for (int i = start; i < end; ++i)
                if (numbers[i] < numbers[elem_position] || (numbers[i] == numbers[elem_position] && elem_position > i))
                {
                    ++position;
                }
            return position;
        }
    }
}
