﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ActressMas;

namespace Proiect
{
    /* This agent sends the start message to a sorting agent so it will start the 
     * sorting a subsequnce from the initial list. It handles the fllowing messages:
     * POSTION old_position new_position: when such a message will be received,
     *                                    from the sorting agent, the gathering agent
     *                                    will keep the new position proposed for 
     *                                    the element.
     * DONE                             : is a message sent by a sorting agent when 
     *                                    it finishes its task. When all the agents 
     *                                    finish their tasks DONE message is sent to 
     *                                    all of them
     */
    class GatheringAgent : Agent
    {
        private List<int> numbers;
        private List<int> finalPostion;
        public List<int> sortedList { get; protected set; }
        protected List<int> startChunk;
        protected List<int> endChunk;
        private int done;       
        private int sentRequests;
        protected int desiredNumberOfAgents;
        protected int numberOfActiveAgents;
 

        public GatheringAgent(List<int> numbers, int desiredNumberOfAgents, int numberOfActiveAgents) 
        {
            this.desiredNumberOfAgents = desiredNumberOfAgents;
            this.numberOfActiveAgents = numberOfActiveAgents;
            this.numbers = numbers;
            this.finalPostion = Utils.CreateFixedSizeList(numbers.Count);
            this.sortedList = Utils.CreateFixedSizeList(numbers.Count);
            ComputeChunksBorder();
        }

        /*
         * Method computes the start end the end position for desiredNumberOfAgents
         * disjoint subsequences for the list of numbers.
         * This method splits the whole list.      
         * ATTENTION: If you do not want to reuse a specific agent after it finishes
         *            its task the desiredNumberOfAgents should be equal to numberOfActiveAgents        
         */
        private void ComputeChunksBorder()
        {
            this.startChunk = new List<int>();
            this.endChunk = new List<int>();
            int current_position = 0;
            int remainder = numbers.Count % desiredNumberOfAgents;
            int division = numbers.Count / desiredNumberOfAgents;
            startChunk.Add(current_position);
            for (int i = 0; i < remainder; ++i)
            {
                current_position += division + 1;
                startChunk.Add(current_position);
                endChunk.Add(current_position);
            }
            for (int i = remainder; i < desiredNumberOfAgents - 1; ++i)
            {
                current_position += division;
                startChunk.Add(current_position);
                endChunk.Add(current_position);
            }
            endChunk.Add(numbers.Count);
        }

        /*
         * This method distributes the computation to all active agents.
         */       
        public override void Setup()
        {
            int remainder = numbers.Count % desiredNumberOfAgents;
            int division = numbers.Count / desiredNumberOfAgents;

            for (int i = 0; i < numberOfActiveAgents; ++i)
            {
                Send( Utils.GetString( Utils.COUNTING_AGENT_PREFIX, i ), Utils.GetString( Utils.ORDER, startChunk[i], endChunk[i] ) );
                Utils.Log("Sending {0} to {1}", Utils.GetString(Utils.ORDER, startChunk[i], endChunk[i]), Utils.GetString(Utils.COUNTING_AGENT_PREFIX, i));
            }

            sentRequests = Utils.NUMBER_OF_ACTIVE_AGENTS;
        }

        /* It handles the message exchange logic and the agent reusage logic
         */       
        public override void Act(Message message)
        {
            string operation;
            List<string> operation_params = new List<string>();

            Utils.ParseMessage(message.Content, out operation, out operation_params);

            Utils.Log("From {0} --> message {1}", message.Sender, message.Content);

            switch (operation)
            {
                case Utils.POSITION:
                    int old_element_position = Convert.ToInt32(operation_params[0]);
                    int new_element_position = Convert.ToInt32(operation_params[1]);
                    sortedList[new_element_position] = numbers[old_element_position];
                    break;

                case Utils.DONE:
                    Interlocked.Increment(ref done);
                    if ( done == startChunk.Count )
                    {
                        HandleDone();
                    }
                    else
                    {
                        SendNextProcessingRequest(message);
                    }
                    break;
            }
        }

        /*
         * This method will be called only if the agent reusage is permitted and
         * it will check if an agent reusage is necessary.        
         */       
        private void SendNextProcessingRequest(Message message)
        {
            if (sentRequests < startChunk.Count)
            {
                string command = Utils.GetString(Utils.ORDER, startChunk[sentRequests], endChunk[sentRequests]);
                Send(message.Sender, command);
                Interlocked.Increment(ref sentRequests);
            }
        }

        /*
         * When all the agents finished their task or all the reused agents finished 
         * their task its method will be called. 
         * 
         * It will broadcast Done message to all the active agents
         * 
         * Depending on the way a new type of agent wants to handle the termination
         * of the task this method will be inherited and modified.        
         */       
        public virtual void HandleDone()
        {
            Broadcast(Utils.DONE);
            Stop();
        }

    }
}
