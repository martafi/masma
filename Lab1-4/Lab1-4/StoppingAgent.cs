﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lab1_4
{
    class StoppingAgent : Agent
    {
        public const int SLEEPING_TIME = 1000;
        public const string STOP_MESSAGE = "STOP";
        private Random _generator = new Random();

        public override void Setup()
        {
            var number_of_alive_agents = Environment.Count();
            while (number_of_alive_agents > 0) 
            {
                var alive_agents_query = Environment.Agents.Where( pair => pair.Value.IsAlive() && pair.Key != "manager" && pair.Key != Name );
                number_of_alive_agents = alive_agents_query.Count();
                if (number_of_alive_agents > 0)
                {
                    string random_agent = alive_agents_query.ToArray().ElementAt(_generator.Next(number_of_alive_agents)).Key;
                    Console.WriteLine($"Sent STOP to {random_agent}");
                    Send(random_agent, STOP_MESSAGE);
                }
                Thread.Sleep(SLEEPING_TIME);
            }
            Stop();
        }
    }
}
