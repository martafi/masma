﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_4
{
    class PingPongAgent : Agent
    {
        private const string PING_MESSAGE = "PING";
        private const string PONG_MESSAGE = "PONG";
        private const string STOP_MESSAGE = "STOP";

        public override void Act(Message message)
        {
            string sender = message.Sender;
            string content = message.Content;

            switch (content)
            {
                case PING_MESSAGE:
                    Console.WriteLine($"Receives Ping from {sender}");
                    Send(sender, PONG_MESSAGE);
                break;
                case STOP_MESSAGE:
                    Console.WriteLine($"Agent {Name} receives STOP from {sender}");
                    Stop();
                break;
                default:
                    throw new Exception("I can't recognize the message");
            }
        }
    }
}
