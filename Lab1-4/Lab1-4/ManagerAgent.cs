﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace Lab1_4
{
    class ManagerAgent : Agent
    {
        private const string PING_MESSAGE = "PING";
        private const string PONG_MESSAGE = "PONG";
        private const string START_MESSAGE = "START";
        private const int SLEEPING_TIME = 1000;
        private Dictionary<string, bool> agents_state = new Dictionary<string, bool>();

        public void AddAgent(string agent_name)
        {
            agents_state.Add(agent_name, true);
        }

        public void _begin()
        {
            //var x = Environment.Agents.Where(pair => agents_state[pair.Key]).ToList();
            //foreach (var y in Environment.Agents)
            //{
            //    Console.WriteLine(y.Key);
            //}
            _reportResponsiveAgents();
            if (Environment.Agents != null)
            {
                //Console.WriteLine("In Begin");
                Environment.Agents.Where(pair => pair.Key != "manager" && pair.Key != "stop" && agents_state[pair.Key]).ToList().ForEach(pair => Send(pair));
            }
           
        }

        private void _reportResponsiveAgents()
        {
            string output = "Responsive agents are ";
            foreach (var item in agents_state.Keys)
            if (agents_state[item])
            {
                output = output + item + " ";
            }
            Console.WriteLine(output);
        }

        private void Send(KeyValuePair<string, Agent> pair)
        {
            agents_state[pair.Key] = false;
            //Console.WriteLine($"Send PING to {pair.Key}");
            Send(pair.Key, PING_MESSAGE);
        }

        public void SendPing()
        {
            Send(Name, START_MESSAGE);
        }

        public bool AnyAlive()
        {
            return agents_state.Values.Any(alive => alive == true);
        }

        public override void Act(Message message)
        {
            switch (message.Content)
            {
                case PONG_MESSAGE:
                    agents_state[message.Sender] = true;
                    //Console.WriteLine($"Received PONG from {message.Sender}");
                    break;
                case START_MESSAGE:
                    _begin(); break;
                default: throw new Exception("Not a message that I know to process");
            }
        }

    }
}
