﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Lab1_4
{
    public class Program
    {
        public static int NUMBER_OF_AGENTS = 5;
        public static string AGENT_NAME = "agent";

        private static void Main(string[] args)
        {
            // === Init ===
            var env = new ActressMas.Environment();

            StoppingAgent stoppingAgent = new StoppingAgent();
            ManagerAgent managerAgent = new ManagerAgent();
            List<PingPongAgent> agents = new List<PingPongAgent>();

            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
            {
                var a = new PingPongAgent();
                env.Add(a, Program.AGENT_NAME + i);
                agents.Add(a);
            }

            env.Add(managerAgent, "manager");
            env.Add(stoppingAgent, "stop");

            managerAgent.Start();
            stoppingAgent.Start();

            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i) { 
                managerAgent.AddAgent(agents[i].Name);
                agents[i].Start();
            }

            while (managerAgent.AnyAlive())
            {
               // Console.WriteLine("Start Sending PING messages");
                managerAgent._begin();
                Thread.Sleep(2000);
            }
            managerAgent.Stop();
        }
    }
}
