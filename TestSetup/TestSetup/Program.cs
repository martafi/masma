﻿using ActressMas;
using System;
using System.Threading;
namespace Agents1
{
    public class Program
    {
        private static void Main(string[] args)
        {
            // === Init ===
            var env = new ActressMas.Environment();
            var a = new MyAgent(); env.Add(a, "a");
            var m = new MonitorAgent(); env.Add(m, "monitor");
            // === Run ===
            m.Start(); a.Start();
            // === Wait ===
            env.WaitAll();
        }
    }
    public class MyAgent : Agent
    {
        private static Random _rand = new Random();
        public override void Setup()
        {
        }

        public override void Act(Message message)
        {
            Send("monitor", "b");
            Console.Write(message.Content);
        }
    }
    public class MonitorAgent : Agent
    {
        public override void Setup()
        {
            Send("a", "a");
        }

        public override void Act(Message message)
        {
            Send("a", "a");
            Console.Write(message.Content);
        }
    }
}