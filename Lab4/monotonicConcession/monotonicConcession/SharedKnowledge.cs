﻿/**************************************************************************
 *                                                                        *
 *  Website:     https://github.com/florinleon/ActressMas                 *
 *  Description: Zeuthen strategy using the ActressMas framework          *
 *  Copyright:   (c) 2018, Florin Leon                                    *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/

using System;

namespace monotonicConcession
{
    public class SharedKnowledge
    {
        public delegate double UtilityFunctionDelegate(double d);

        public static double Utility1(double deal)
        {
            return 5.0 - deal;
        }

        public static double Utility2(double deal)
        {
            return 2.0 / 3.0 * deal;
        }

        public static double Utility11(double deal)
        {
            return 5.0 - deal;
        }

        public static double Utility21(double deal)
        {
            return 200 * Math.Sqrt(deal);
        }


        public static double NewProposal1(double my, double others, UtilityFunctionDelegate Utility1, UtilityFunctionDelegate Utility2)
        {
            for (double d = my; d <= others; d += Utils.Eps)
                if (Utility2(d) >  Utility2(my) && Utility1(d) > Utility1(others) )
                    return d;

            return others;
        }

        public static double NewProposal2(double d2, double d1, UtilityFunctionDelegate Utility2, UtilityFunctionDelegate Utility1)
        {
            for (double d = d2; d >= d1; d -= Utils.Eps)
                if (Utility1(d) > Utility1(d2) && Utility2(d) > Utility2(d1))
                    return d;

            return d1;
        }
    }
}