﻿/**************************************************************************
 *                                                                        *
 *  Website:     https://github.com/florinleon/ActressMas                 *
 *  Description: The reactive architecture using the ActressMas framework *
 *  Copyright:   (c) 2018, Florin Leon                                    *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Windows.Forms;

namespace Reactive
{
    public class PlanetAgent : ActressMas.Agent
    {
        private PlanetForm _formGui;
        public Dictionary<string, string> ExplorerPositions { get; set; }
        public Dictionary<string, string> ResourcePositions { get; set; }
        public Dictionary<string, int> ResourcePath { get; set; }
        public Dictionary<string, string> Loads { get; set; }
        private string _basePosition;

        public PlanetAgent()
        {
            ExplorerPositions = new Dictionary<string, string>();
            ResourcePositions = new Dictionary<string, string>();
            ResourcePath = new Dictionary<string, int>();
            Loads = new Dictionary<string, string>();
            _basePosition = Utils.Str(Utils.Size / 2, Utils.Size / 2);

            Thread t = new Thread(new ThreadStart(GUIThread));
            t.Start();
        }

        private void GUIThread()
        {
            _formGui = new PlanetForm();
            _formGui.SetOwner(this);
            _formGui.ShowDialog();
            Application.Run();
        }

        private void _randomResourcePosition()
        {
            Console.WriteLine("Starting " + Name);

            List<string> resPos = new List<string>();
            string compPos = Utils.Str(Utils.Size / 2, Utils.Size / 2);
            resPos.Add(compPos); // the position of the base

            for (int i = 1; i <= Utils.NoResources; i++)
            {
                while (resPos.Contains(compPos)) // resources do not overlap
                {
                    int x = Utils.RandNoGen.Next(Utils.Size);
                    int y = Utils.RandNoGen.Next(Utils.Size);
                    compPos = Utils.Str(x, y);
                }

                ResourcePositions.Add("res" + i, compPos);
                resPos.Add(compPos);
            }
        }

        private void _clusteredResourcesPositions()
        {
            Console.WriteLine("Starting " + Name);

            List<string> resPos = new List<string>();
            string compPos = Utils.Str(Utils.Size / 2, Utils.Size / 2);
            int lim = Convert.ToInt32(Math.Sqrt(Utils.NoResources)) + 1;
            resPos.Add(compPos); // the position of the base

            for (int i = 1; i <= Utils.NoResources; i++)
            {
                while (resPos.Contains(compPos)) // resources do not overlap
                {
                    int x = Utils.RandNoGen.Next(lim);
                    int y = Utils.RandNoGen.Next(lim);
                    compPos = Utils.Str(x, y);
                }

                ResourcePositions.Add("res" + i, compPos);
                resPos.Add(compPos);
            }
        }

        public override void Setup()
        {
            _clusteredResourcesPositions();
            foreach (var resource in ResourcePositions.Keys) {
                ResourcePath.Add(ResourcePositions[resource], Utils.NoResources * 2);
            }
        }

        public override void Act(ActressMas.Message message)
        {
            try
            {
                Console.WriteLine("\t[{1} -> {0}]: {2}", this.Name, message.Sender, message.Content);

                string action; string parameters;
                Utils.ParseMessage(message.Content, out action, out parameters);

                switch (action)
                {
                    case "position":
                        HandlePosition(message.Sender, parameters);
                        break;

                    case "change":
                        HandleChange(message.Sender, parameters);
                        break;

                    case "pick-up":
                        HandlePickUp(message.Sender, parameters);
                        break;

                    case "carry":
                        HandleCarry(message.Sender, parameters);
                        break;

                    case "unload":
                        HandleUnload(message.Sender, parameters);
                        break;

                    default:
                        break;
                }

                _formGui.UpdatePlanetGUI();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void HandlePosition(string sender, string position)
        {
            ExplorerPositions.Add(sender, position);
            Send(sender, "move -1");
        }

        private void HandleChange(string sender, string position)
        {
            ExplorerPositions[sender] = position;

            if (ResourcePath.ContainsKey(position))
            {
                ResourcePath[position]--;
                if (ResourcePath[position] == 0)
                {
                    ResourcePath.Remove(position);
                }
            }

            foreach (string k in ExplorerPositions.Keys)
            {
                if (k == sender)
                    continue;
                if (ExplorerPositions[k] == position)
                {
                    Send(sender, "block");
                    return;
                }
            }

            foreach (string k in ResourcePositions.Keys)
            {
                if (position != _basePosition && ResourcePositions[k] == position)
                {
                    Send(sender, "rock " + k);
                    return;
                }
            }
            int future_position = _computeFutureDirection(position);
            Send(sender, "move " + Convert.ToString( future_position));
        }

        private int _pathValueOnDirection(int _x, int _y, int d)
        {
            bool modified = false;
            switch (d)
            {
                case 0: if (_x > 0) _x--; modified = true; break;
                case 1: if (_x < Utils.Size - 1) _x++; modified = true;  break;
                case 2: if (_y > 0) _y--; modified = true; break;
                case 3: if (_y < Utils.Size - 1) _y++; modified = true; break;
            }
            string position = Utils.Str(_x, _y);
            if (!modified || !ResourcePath.ContainsKey(position))
            {
                return 0;
            }
            return ResourcePath[position];
        }

        private int _computeFutureDirection(string position)
        {
            int x = Convert.ToInt32(position.Split()[0]);
            int y = Convert.ToInt32(position.Split()[1]);

            int mx = 0, dmx = -1;
            for (int d = 0; d < 4; ++d)
            {
                int res = _pathValueOnDirection(x, y, d);
                if (res > mx)
                {
                    mx = res;
                    dmx = d;
                }

            }
            return dmx;
        }

        private void HandlePickUp(string sender, string resource)
        {
            Loads[sender] = resource;
            if (!ResourcePath.ContainsKey(ResourcePositions[resource]))
            {
                ResourcePath.Add(ResourcePositions[resource], 0);
            }
            ResourcePath[ResourcePositions[resource]] += 2;
            Send(sender, "move");
        }

        private void HandleCarry(string sender, string position)
        {
            ExplorerPositions[sender] = position;
            string res = Loads[sender];
            ResourcePositions[res] = position;
            if (!ResourcePath.ContainsKey(position))
            {
                ResourcePath.Add(position, 0);
            }
            ResourcePath[position] += 2;
            Send(sender, "move");
        }

        private void HandleUnload(string sender, string resource)
        {
            Loads.Remove(sender);
            ResourcePositions.Remove(resource);
            if (ResourcePositions.Count != 0)
            {
                Send(sender, "move -1");
            }
            else
            {
                BroadcastAll("stop");
                Stop();
            }
              
        }
    }
}