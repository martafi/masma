﻿/**************************************************************************
 *                                                                        *
 *  Website:     https://github.com/florinleon/ActressMas                 *
 *  Description: English auction without broadcast using the ActressMas   *
 *               framework                                                *
 *  Copyright:   (c) 2018, Florin Leon                                    *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/

using ActressMas;
using System;

namespace Vickrey
{
    public class BidderAgent : Agent
    {
        private Random _rand;

        public BidderAgent()
        {
            _rand = new Random();
        }

        public override void Act(Message message)
        {
            try
            {
                Console.WriteLine("\t[{1} -> {0}]: {2}", this.Name, message.Sender, message.Content);

                string action; string parameters;
                Utils.ParseMessage(message.Content, out action, out parameters);

                switch (action)
                {
                    case "price":
                        HandlePrice();
                        break;

                    case "winner":
                        HandleWinner(parameters);
                        break;
                    case "stop":
                        Stop();
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void HandlePrice()
        {
            int my_price = Utils.MarketPrice + _rand.Next(Utils.Variance * 2) - 100;
            
            Send("auctioneer", Utils.Str("bid", my_price));
        }

        private void HandleWinner(string winner)
        {
            if (winner == this.Name)
                Console.WriteLine("[{0}]: I have won.", this.Name);
        }
    }
}