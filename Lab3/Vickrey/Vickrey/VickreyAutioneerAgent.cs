﻿using ActressMas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace Vickrey
{
    public class VickreyAutioneerAgent : Agent
    {
        private List<KeyValuePair<string,int>> _bidders;
        private int _rounds = 1;

        public List<int> _winningPrices { get; }


        public VickreyAutioneerAgent()
        {
            _bidders = new List<KeyValuePair<string,int>>();
            _winningPrices = new List<int>();
        }

        public override void Setup()
        {
            Broadcast("price");
        }

        public override void Act(Message message)
        {
            try
            {
               // Console.WriteLine("\t[{1} -> {0}]: {2}", this.Name, message.Sender, message.Content);

                string action;
                string parameters;
                Utils.ParseMessage(message.Content, out action, out parameters);

                switch (action)
                {
                    case "bid":
                        int price = Convert.ToInt32(parameters);
                        HandleBid(message.Sender, price);
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void HandleBid(string sender, int price)
        {
            _bidders.Add(new KeyValuePair<string, int>(sender, price));
            if (_bidders.Count == Environment.NoAgents - 1)
            {
                var _orderdBidders = _bidders.OrderBy(x => -1 * x.Value).ToArray();
                string winner = _orderdBidders[0].Key;
                int winning_price = _orderdBidders[1].Value;

                _winningPrices.Add(winning_price);

                Broadcast(Utils.Str("winner", winner));

                Console.WriteLine($"\t\tThe winner is {winner} and the price that he is paying is {winning_price}");

                _bidders.Clear();

                if (_rounds < Utils.noRounds)
                {
                    Broadcast("price");
                    _rounds++;
                } else
                {
                    Broadcast("stop");
                    _DisplayResults();
                    Stop();
                }
            }
        }

        private void _DisplayResults()
        {
            var _orderedWinningPrices = _winningPrices.OrderBy(x => x).ToArray();
            List<KeyValuePair<int, int>> histogram = new List<KeyValuePair<int, int>>();

            int j = 0;
            for (int price = Utils.MarketPrice - Utils.Variance; j < _orderedWinningPrices.Length && price < Utils.MarketPrice + Utils.Variance; price += 10)
            {
                int nr = 0;
                for (; j < _orderedWinningPrices.Length && _orderedWinningPrices[j] < price + 10; ++j)
                {
                    ++nr;
                   // Console.WriteLine($"{_orderedWinningPrices[j]}        {price}");
                }
                histogram.Add(new KeyValuePair<int, int>(price, nr));
                Console.WriteLine($"Between {price} and {price + 9} there are {nr} winning prices the probability for such a price is {1.0 * nr / 1000.0}.");
            }


        }
    }
}