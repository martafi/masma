﻿using ActressMas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Agents1
{
    public class Program
    {
        private static int NUMBER_OF_AGENTS = 4;
        public static int NUMBER_OF_ITERATIONS = 5;
        private static void Main(string[] args)
        {
            // === Init ===
            var env = new ActressMas.Environment();
            List<Agent> agents = new List<Agent>();

            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
            {
                var a = new MyAgent();
                env.Add(a, "agent" + i);
                agents.Add(a);
            }
            // === Run ===
            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
            {
                agents[i].Start();
            }
            // === Wait ===
            Console.WriteLine("We will wait");
            env.WaitAll();
            Console.WriteLine("We will finish waiting");
        }
    }
    public class MyAgent : Agent
    {
        private static Random _rand = new Random();
        public override void Setup()
        {
            for (int i = 0; i < Program.NUMBER_OF_ITERATIONS; i++)
            {
                Console.WriteLine("Agent {0} counted untill {1}", this.Name, i);
               // Send("monitor", i.ToString());
                int waiting_time = 500 + _rand.Next(4500);
                Thread.Sleep(waiting_time);
            }
            Console.WriteLine("Agent {0} is no longer needed", this.Name);
            Stop();
        }
    }
}