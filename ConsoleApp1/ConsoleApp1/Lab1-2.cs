﻿using ActressMas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Agents2
{
    public class Program
    {
        private static int NUMBER_OF_AGENTS = 2;
        public static int NUMBER_OF_ITERATIONS = 100;
        public static int NUMBER_OF_REPETITIONS = 10;
        //private static void Main(string[] args)
        //{
        //    // === Init ===
        //    var env = new ActressMas.Environment();
        //    List<Agent> agents = new List<Agent>();

        //    for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
        //    {
        //        var a = new MyAgent();
        //        env.Add(a, "agent" + i);
        //        agents.Add(a);
        //    }
        //    // === Run ===
        //    for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
        //    {
        //        agents[i].Start();
        //    }
        //    // === Wait ===
        //    Console.WriteLine("We will wait");
        //    env.WaitAll();
        //    Console.WriteLine("We will finish waiting");
        //    Thread.Sleep(50000);
        //}
    }
    public class MyAgent : Agent
    {
        private static Random _rand = new Random();

        public override void Setup()
        {
            int sum = 0;
            for (int i = 0; i < Program.NUMBER_OF_REPETITIONS; ++i)
            {
                for (int j = 0; j < Program.NUMBER_OF_ITERATIONS; ++j)
                {
                    int number = 1 + _rand.Next(10);
                    sum += number;
                }
                double average = sum * 1.0 / ((i + 1) * Program.NUMBER_OF_ITERATIONS);
                Console.WriteLine("Agent {0} processed {1} the partial result {2:N4}", Name, (i + 1) * Program.NUMBER_OF_ITERATIONS, average);
            }
            Console.WriteLine("Agent {0} is no longer needed", this.Name);
            Stop();
        }
    }
}