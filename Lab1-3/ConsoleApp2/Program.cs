﻿using ActressMas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Agents
{
    public class Program
    {
        public static int NUMBER_OF_AGENTS = 5;
        public static string AGENT_NAME = "agent";

        private static void Main(string[] args)
        {
            // === Init ===
            var env = new ActressMas.Environment();
          
            List<MyAgent> agents = new List<MyAgent>();

            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
            {
                var a = new MyAgent(env);
                env.Add(a, Program.AGENT_NAME + i);
                agents.Add(a);
            }
            // === Run ===
            for (int i = 0; i < Program.NUMBER_OF_AGENTS; ++i)
            {
                agents[i].Start();
            }
            
            Console.WriteLine("We will wait");
            env.WaitAll();
            Console.WriteLine("We finished waiting");
            //Thread.Sleep(5000);
        }
    }
    public class MyAgent : Agent
    { 
        private static Random _rand = new Random();
        private static string QUESTION = "What time is it?";
        private static string ANSWER = "Time is ";
        private static string DONE = "Done";

        int done_messages_counts = 0;
        ActressMas.Environment env;

        public MyAgent(ActressMas.Environment env)
        {
            this.env = env;
        }

        public override void Setup()
        {
            string agent_name = Program.AGENT_NAME + _rand.Next(Program.NUMBER_OF_AGENTS);
            agent_name = Program.AGENT_NAME + _rand.Next(Program.NUMBER_OF_AGENTS);
            Send(agent_name, QUESTION);
        }

        public override void Act(Message message)
        {
            string sender = message.Sender;
            string message_content = message.Content;

            if (string.Equals(message_content, DONE))
            {
                ++done_messages_counts;
                if (done_messages_counts == Program.NUMBER_OF_AGENTS - 1)
                {
                    //Console.WriteLine("Stoping " + Name);
                    Stop();
                }
                //Console.WriteLine("Agent {0} receives done message from {1}", Name, sender);
            } else if (string.Equals(message_content, QUESTION))
            {
                Console.WriteLine("[{0} is asking] {1}", sender, QUESTION);
                DateTime now = DateTime.Now;
                Send(sender, MyAgent.ANSWER + " " + now.ToShortTimeString());
            } else if (message_content.StartsWith(MyAgent.ANSWER))
            {
                Console.WriteLine("[{0} is answering] {1}", sender, message_content);
                BroadcastAll(DONE);
            } else 
            {
                Console.WriteLine("Unrecognisable message.");
            }
        }
    }
}